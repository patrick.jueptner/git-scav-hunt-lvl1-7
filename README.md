Herzlich Willkommen bei der Git-Schnitzeljagd!

Clone dieses Repository lokal auf deinen Rechner.

Level 1:
Die erste Aufgabe lautet:

Checke den Commit aus, der die Antwort auf die Frage nach dem Sinn des Lebens, dem Universum und dem ganzen Rest beantwortet!

Alles weitere ergibt sich aus den Inhalten des Arbeitsverzeichnisses, daher kann es nützlich sein einen Blick in die Dateien des jeweiligen Commits zu werfen.

-------------

15/03/2024 - Lifecycle update
